import { Routes } from '@angular/router';

export const APP_ROUTES: Routes = [
  {
    path: 'weather',
    loadChildren: () =>
      import('@blend-api/weather').then((r) => r.WEATHER_ROUTES),
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '',
  },
];
