## Local Setup with nx

1. Run `npm install -g @nrwl/cli` to install nx.

2. Navigate to the project root directory /app-meteo-v2.

3. Run `npm install` to install all project dependencies and generate the Nx workspace configuration.

4. To start the app, run `nx serve` from the project root directory. The app will open at `http://localhost:4200/`.
