import { TemperatureMeasurementUnits } from '../types/weather.interface';

export const WEATHER_API_CONFIG: {
  URL: string;
  API_KEY: string;
  UNITS: TemperatureMeasurementUnits;
} = {
  URL: 'https://api.openweathermap.org/data/2.5/forecast',
  API_KEY: 'd346575e70d607e1108f0279b6abd5b6',
  UNITS: 'metric',
};
