import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ElementRef,
  ViewChild,
  AfterViewInit,
  Renderer2,
  inject,
  OnDestroy,
} from '@angular/core';
import { LogoLink, NavbarMenu } from '../../types/navbar.interface';
import { FeatherModule } from 'angular-feather';
import { RouterLink, RouterLinkActive } from '@angular/router';

@Component({
  selector: 'ba-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [RouterLink, RouterLinkActive, FeatherModule],
})
export class NavbarComponent implements AfterViewInit, OnDestroy {
  @Input() navbarMenuItems: NavbarMenu[];
  @Input() appLogoWithLink: LogoLink;
  @ViewChild('navbarBurger') navbarBurger: ElementRef;
  @ViewChild('appNavBarMenu') appNavBarMenu: ElementRef;

  #renderer: Renderer2 = inject(Renderer2);
  #clickEventListenerOnNavbarBurger: Function;
  #clickEventListenerOnAppNavbarMenu: Function;

  private toggleActiveClassOnNavbarBurgerClick() {
    const navbarBurger = this.navbarBurger.nativeElement;
    const appNavbarMenu = this.appNavBarMenu.nativeElement;
    if (navbarBurger.className.includes('is-active')) {
      this.#renderer.removeClass(navbarBurger, 'is-active');
      this.#renderer.removeClass(appNavbarMenu, 'is-active');
    } else {
      this.#renderer.addClass(navbarBurger, 'is-active');
      this.#renderer.addClass(appNavbarMenu, 'is-active');
    }
  }

  ngAfterViewInit(): void {
    this.#clickEventListenerOnNavbarBurger = this.#renderer.listen(
      this.navbarBurger.nativeElement,
      'click',
      () => this.toggleActiveClassOnNavbarBurgerClick(),
    );

    // event delegation
    this.#clickEventListenerOnAppNavbarMenu = this.#renderer.listen(
      this.appNavBarMenu.nativeElement,
      'click',
      (event: { target: HTMLAnchorElement }) => {
        this.toggleActiveClassOnNavbarBurgerClick(); // needed on mobile and tablet.
        event.target.blur(); // blurs the target (menu item, where the mouse event originated)
      },
    );
  }

  ngOnDestroy(): void {
    this.#clickEventListenerOnNavbarBurger();
    this.#clickEventListenerOnAppNavbarMenu();
  }
}
