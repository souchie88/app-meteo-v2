import { LogoLink, NavbarMenu } from '../types/navbar.interface';

export const LOGO_WITH_LINK: LogoLink = {
  logo: '../../assets/images/gulli.png',
  routerLink: '/',
};

export const NAVBAR_MENU_ITEMS: Array<NavbarMenu> = [
  {
    title: 'Weather',
    hasSubMenu: false,
    icon: 'cloud-lightning',
    routerLink: '/weather',
  },
  {
  title: 'Infos',
  hasSubMenu: false,
  icon: 'eye',
  routerLink: '/infos',
  }
];
